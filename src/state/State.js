

export const state = [

    {
        "id": "1",
        "name": "User1",
        "surname":"UserSurname1",
        "age": 20,
        "city": "Moskow",
        "street": "MoskowStreet",
        "house": 22+'/'+2,
        "email": "Moskow@mail.ru",
        "education": "BSTU",

    },
    {
        "id": "2",
        "name": "User2",
        "surname": "UserSurname2",
        "age": 19,
        "city": "Tokyo",
        "street": "TokyoStreet",
        "house": 102,
        "email": "Tokyo@mail.ru",
        "education": "TokyoHighestUniversity",

    },
    {
        "id": "3",
        "name": "User3",
        "surname": "UserSurname3",
        "age": 26,
        "city": "NY",
        "street": "WaalStreet",
        "house": 103+'b',
        "email": "NY@mail.ru",
        "education": "Cambridge",

    }

]

