import PostSubmit from "./components/PostSubmit";
import {BrowserRouter, Route} from "react-router-dom";
import { PageAuthorsAll, AuthorPost } from "./components/AuthorPost";


function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Route exact path='/' component={PostSubmit}/>
                <Route exact path='/authors/' component={PageAuthorsAll}/>
                <Route path='/authors/User1 UserSurname1' component={AuthorPost.bind(this, 0)}/>
                <Route path='/authors/User2 UserSurname2' component={AuthorPost.bind(this, 1)}/>
                <Route exact path='/authors/User3 UserSurname3' component={AuthorPost.bind(this, 2)}/>

            </div>
        </BrowserRouter>
    );
}

export default App;
