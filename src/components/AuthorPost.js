import React from "react";
import {state} from "../state/State";
import {NavLink} from "react-router-dom";
import style from './Style/Styles/AuthorPost.module.css'


export function PageAuthorsAll() {
    return (
        state.map((item) => {
                return (
                    <>
                        <div className={style.allAuthorsPage}>
                            <NavLink to="/">Back</NavLink>
                            <div className={style.authorsBlock}>

                                <h1>{item.name + '' + item.surname}</h1>
                                <p>Age: {item.age}</p>
                                <p>Adress: город {item.city}, улица {item.street}, дом {item.house}</p>
                                <p>Mail: {item.email}</p>
                                <p>Aducation: {item.education}</p>
                            </div>
                        </div>
                    </>
                )
            }
        )
    )
}

export function AuthorPost(id) {
    return (
        <>
            <div className={style.authorsBlock}>
                <h1>{state[id].name + '' + state[id].surname}</h1>
                <p>Age: {state[id].age}</p>
                <p>Adress: город {state[id].city}, улица {state[id].street}, дом {state[id].house}</p>
                <p>Mail: {state[id].email}</p>
                <p>Aducation: {state[id].education}</p>
                <NavLink to="/">Back</NavLink>
            </div>
        </>
    )

}

