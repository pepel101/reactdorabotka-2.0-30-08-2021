import React, {useState} from "react";
import style from './Style/Styles/PostForma.module.css'


function PostForma(props) {

    const [valueSelect, setValueSelect] = useState( props.edit ? props.edit.value_author : '');
    const [valueHeading, setValueHeading] = useState(props.edit ? props.edit.value_heading : '');
    const [valuePostText, setValuePostText] = useState(props.edit ? props.edit.value_text : '');

    const selectOptions = props.state.map((item) => {
        return (<option>{item.name + ' ' + item.surname}</option>)
    })

    const handleChangeSelect = e => {
        setValueSelect(e.target.value)
    }
    const handleChangeHeading = e => {
        setValueHeading(e.target.value)
    }
    const handleChangePostText = e => {
        setValuePostText(e.target.value)
    }
    const handleSubmit = e => {
        e.preventDefault();
        props.onSubmit({
            id: Math.floor(Math.random() * 1000),
            author: valueSelect,
            heading: valueHeading,
            text: valuePostText,
            postDate: new Date().toLocaleTimeString()
        })


        setValueSelect('')
        setValueHeading('')
        setValuePostText('')
    }

    return (
        <form  onSubmit={handleSubmit}>
            {props.edit ? (
                <>
                    <h2> MY NEWSxs.</h2>
                    <select onChange={handleChangeSelect} name="select" id="select" value={valueSelect}>
                        <option>Change autor</option>
                        {selectOptions}
                    </select>
                    <input value={valueHeading} onChange={handleChangeHeading} type="text" className={style.headingPost}
                           name="headingPost" placeholder="Enter your  new title..." maxLength="40"/>
                    <textarea value={valuePostText} onChange={handleChangePostText} className={style.postText} name="postText"
                              placeholder="Your new news..."rows="5"/>
                    <button type="submit" className={style.sendPost}>Submit</button>
                </>
            ) : (
                <>
                    <h2>MY NEWS.</h2>
                    <select onChange={handleChangeSelect} name="select" id="select" value={valueSelect}>
                        <option>Change autor</option>
                        {selectOptions}
                    </select>
                    <input value={valueHeading} onChange={handleChangeHeading} type="text" className={style.headingPost}
                           name="headingPost" placeholder="Enter your title..." maxLength="60"/>
                    <textarea value={valuePostText} onChange={handleChangePostText} className={style.postText} name="postText"
                              placeholder="Your news..." rows="5"/>
                    <button type="submit" className={style.sendPost}>Send</button>
                </>
            )}
        </form>
    )

}

export default PostForma;