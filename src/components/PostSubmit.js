import React, {useState} from "react";
import PostForma from "./PostForma";
import Post from "./Post";
import {state} from "../state/State";
import {StopButton} from "./StopButton";
import style from './Style/Styles/PostSubmit.module.css'


function PostSubmit() {
    const [posts, setPosts] = useState([])

    const addPost = post => {
        const allPosts = [post, ...posts]
        setPosts(allPosts);
    }

    const editPost = (postId, newValue) => {
        setPosts(prev => prev.map(item => (item.id === postId ? newValue : item)))
    }

    const removePost = (id) => {
        const newPosts = [...posts].filter(post => post.id !== id)
        setPosts(newPosts)
    }

    const sortUp = (fieldName) => {
        const sortPost = [...posts].sort(function (a, b) {
            return a[fieldName] < b[fieldName] ? -1 : a[fieldName] > b[fieldName] ? 1 : 0
        })
        setPosts(sortPost)
    }

    const sortDown = (fieldName) => {
        const sortPost = [...posts].sort(function (a, b) {
            return a[fieldName] > b[fieldName] ? -1 : a[fieldName] < b[fieldName] ? 1 : 0
        })
        setPosts(sortPost)
    }

    return (
        <div className="mainDiv">
            <div className={style.blockH1}>
                <h1>News feed</h1>
            </div>
            <div className={style.actionBlock}>
                <div>
                    <PostForma onSubmit={addPost}
                               state={state}/>
                    <StopButton
                        sortUp={sortUp}
                        sortDown={sortDown}
                    />
                </div>
                <div className={style.blockPosts}>

                    <Post posts={posts}
                          removePost={removePost}
                          editPost={editPost}
                          state={state}
                    />
                </div>
            </div>
        </div>
    )
}

export default PostSubmit