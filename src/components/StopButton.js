export function StopButton({ sortUp, sortDown }) {
    return (
        <div className="blockButton">
            <h3>Sort By:</h3>
            <div>
                <span>Autor</span>
                <button onClick={sortUp.bind(this, 'author')} type="button" className="circleRed">+</button>
                <button onClick={sortDown.bind(this, 'author')} type="button" className="circleGreen">-</button>
            </div>
            <div>
                <span>Title </span>
                <button onClick={sortUp.bind(this, 'heading')} type="button" className="circleRed">+</button>
                <button onClick={sortDown.bind(this, 'heading')} type="button" className="circleGreen">-</button>
            </div>
            <div>
                <span>Text </span>
                <button onClick={sortUp.bind(this, 'text')} type="button" className="circleRed">+</button>
                <button onClick={sortDown.bind(this, 'text')} type="button" className="circleGreen">-</button>
            </div>
            <div>
                <span>Date </span>
                <button onClick={sortUp.bind(this, 'postDate')} type="button" className="circleRed">+</button>
                <button onClick={sortDown.bind(this, 'postDate')} type="button" className="circleGreen">-</button>
            </div>
        </div>
    )
}